package com.pentatools.serviceuser.application.service;

import com.pentatools.serviceuser.domain.models.DTOUser;
import com.pentatools.serviceuser.domain.ports.in.UserUseCase;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public class UserService implements UserUseCase {

    private final UserUseCase userUseCase;

    public UserService(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    @Override
    public DTOUser createUser(DTOUser dtoUser) {
        return userUseCase.createUser(dtoUser);
    }

    @Override
    public Optional<DTOUser> getUser(Long id) {
        return userUseCase.getUser(id);
    }

    @Override
    public List<DTOUser> getAllUser() {
        return userUseCase.getAllUser();
    }

    @Override
    public Optional<DTOUser> updateUser(Long id, DTOUser dtoUser) {
        return userUseCase.updateUser(id, dtoUser);
    }

    @Override
    public boolean deleteUser(Long id) {
        return userUseCase.deleteUser(id);
    }
}

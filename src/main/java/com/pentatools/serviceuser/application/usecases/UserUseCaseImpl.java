package com.pentatools.serviceuser.application.usecases;

import com.pentatools.serviceuser.domain.models.DTOUser;
import com.pentatools.serviceuser.domain.ports.in.UserUseCase;
import com.pentatools.serviceuser.domain.ports.out.UserRepositoryPort;

import java.util.List;
import java.util.Optional;

public class UserUseCaseImpl implements UserUseCase {

    private final UserRepositoryPort userRepositoryPort;

    public UserUseCaseImpl(UserRepositoryPort userRepositoryPort) {
        this.userRepositoryPort = userRepositoryPort;
    }
    @Override
    public DTOUser createUser(DTOUser dtoUser) {
        return userRepositoryPort.save(dtoUser);
    }

    @Override
    public Optional<DTOUser> getUser(Long id) {
        return userRepositoryPort.findById(id);
    }

    @Override
    public List<DTOUser> getAllUser() {
        return userRepositoryPort.findAll();
    }

    @Override
    public Optional<DTOUser> updateUser(Long id, DTOUser dtoUser) {
        return userRepositoryPort.update(dtoUser);
    }

    @Override
    public boolean deleteUser(Long id) {
        return userRepositoryPort.deleteById(id);
    }
}

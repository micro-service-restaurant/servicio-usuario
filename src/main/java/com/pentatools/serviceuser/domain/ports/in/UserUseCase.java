package com.pentatools.serviceuser.domain.ports.in;

import com.pentatools.serviceuser.domain.models.DTOUser;

import java.util.List;
import java.util.Optional;

public interface UserUseCase {
    DTOUser createUser(DTOUser dtoUser);

    Optional<DTOUser> getUser(Long id);

    List<DTOUser> getAllUser();

    Optional<DTOUser> updateUser(Long id, DTOUser dtoUser);

    boolean deleteUser(Long id);
}

package com.pentatools.serviceuser.domain.ports.out;

import com.pentatools.serviceuser.domain.models.DTOUser;

import java.util.List;
import java.util.Optional;

public interface UserRepositoryPort {
    DTOUser save(DTOUser dtoUser);
    Optional<DTOUser> findById(Long id);

    List<DTOUser> findAll();
    Optional<DTOUser> update(DTOUser dtoUser);
    boolean deleteById(Long id);
}

package com.pentatools.serviceuser.domain.models;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DTOUser {

    private Long id;

    @Column(length = 20)
    private String userName;

    private String email;

    private String information;

    public DTOUser(Long id, String userName, String email, String information) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.information = information;
    }
}

package com.pentatools.serviceuser.infraestructure.entities;

import com.pentatools.serviceuser.domain.models.DTOUser;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20)
    private String userName;

    private String email;

    private String information;

    public UserEntity(Long id, String userName, String email, String information) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.information = information;
    }

    public static UserEntity fromDomainModel(DTOUser dtoUser){
        return new UserEntity(dtoUser.getId(), dtoUser.getUserName(), dtoUser.getEmail(), dtoUser.getInformation());
    }

    public DTOUser toDomainModel(){
        return new DTOUser(id, userName,email, information);
    }

}

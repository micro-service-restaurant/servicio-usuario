package com.pentatools.serviceuser.infraestructure.repositories;

import com.pentatools.serviceuser.infraestructure.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
}

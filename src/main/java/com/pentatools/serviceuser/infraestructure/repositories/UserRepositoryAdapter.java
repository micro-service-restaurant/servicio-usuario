package com.pentatools.serviceuser.infraestructure.repositories;

import com.pentatools.serviceuser.domain.models.DTOUser;
import com.pentatools.serviceuser.domain.ports.out.UserRepositoryPort;
import com.pentatools.serviceuser.infraestructure.entities.UserEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserRepositoryAdapter implements UserRepositoryPort {

    private final UserRepository userRepository;

    public UserRepositoryAdapter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public DTOUser save(DTOUser dtoUser) {
        UserEntity userEntity = UserEntity.fromDomainModel(dtoUser);
        UserEntity savedUserEntity = userRepository.save(userEntity);
        return savedUserEntity.toDomainModel();
    }

    @Override
    public Optional<DTOUser> findById(Long id) {
        return userRepository.findById(id).map(UserEntity::toDomainModel);
    }

    @Override
    public List<DTOUser> findAll() {
        return userRepository.findAll().stream().map(UserEntity::toDomainModel)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<DTOUser> update(DTOUser dtoUser) {
        if (userRepository.existsById(dtoUser.getId())){
            UserEntity taskEntity = UserEntity.fromDomainModel(dtoUser);
            UserEntity updateTaskEntity = userRepository.save(taskEntity);
            return Optional.of(updateTaskEntity.toDomainModel());
        }
        return Optional.empty();
    }

    @Override
    public boolean deleteById(Long id) {
        if (userRepository.existsById(id)){
            userRepository.deleteById(id);
            return true;
        }
        return false;
    }
}

package com.pentatools.serviceuser.infraestructure.config;


import com.pentatools.serviceuser.application.service.UserService;
import com.pentatools.serviceuser.application.usecases.UserUseCaseImpl;
import com.pentatools.serviceuser.domain.ports.out.UserRepositoryPort;
import com.pentatools.serviceuser.infraestructure.repositories.UserRepositoryAdapter;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfig {


    @Bean
    public UserService userService(UserRepositoryPort userRepositoryPort){
        return new UserService(
                new UserUseCaseImpl(userRepositoryPort)
                //new RetrieveTaskUseCaseImpl(taskRepositoryPort),
                //new UpdateTaskUseCaseImpl(taskRepositoryPort),
                //new DeleteTaskUseCaseImpl(taskRepositoryPort),
                //getAdditionalTaskInfoUseCase
        );
    }

    @Bean
    public UserRepositoryPort userRepositoryPort(UserRepositoryAdapter userRepositoryAdapter) {
        return userRepositoryAdapter;
    }

    /*@Bean
    public GetAdditionalTaskInfoUseCase getAdditionalTaskInfoUseCase(ExternalServicePort externalServicePort) {
        return new GetAdditionalTaskInfoUseCaseImpl(externalServicePort);
    }*/

    /*@Bean
    public ExternalServicePort externalServicePort() {
        return new ExternalServiceAdapter();
    }*/

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}

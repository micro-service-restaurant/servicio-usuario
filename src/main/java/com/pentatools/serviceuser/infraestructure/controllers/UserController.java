package com.pentatools.serviceuser.infraestructure.controllers;

import com.pentatools.serviceuser.application.service.UserService;
import com.pentatools.serviceuser.domain.models.DTOUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/service-user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<DTOUser> createUser(@RequestBody DTOUser dtoUser){
        DTOUser createdUser = userService.createUser(dtoUser);
        return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<DTOUser> getUserId(@PathVariable Long userId){
        return userService.getUser(userId)
                .map(user-> new ResponseEntity<>(user, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public ResponseEntity<List<DTOUser>> getAllUser(){
        List<DTOUser> users = userService.getAllUser();
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PutMapping("/{userId}")
    public ResponseEntity<DTOUser> updateUser(@PathVariable Long userId, @RequestBody DTOUser dtoUser){
        return userService.updateUser(userId, dtoUser)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteUserById(@PathVariable Long userId){
        if (userService.deleteUser(userId)){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
